package com.book.application.controller;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.events.Event.ID;

import com.book.application.dto.ResponseDTO;
import com.book.application.entity.Book;
import com.book.application.service.BookService;

@RestController
@RequestMapping("books")
public class BookController {
	

	@Autowired
	private BookService service;
	
	@Autowired
    ResourceBundleMessageSource messageSource;
	
	@GetMapping("/{locale}/test")
	public String testing(@PathVariable("locale") Locale locale) {
		return messageSource.getMessage("applation.message.name",null,locale);
		
	}
	
	@GetMapping("/getMessage")
    public String getLocaleMessage(@RequestHeader(name="Accept-Language", required=false) Locale locale) {
        return messageSource.getMessage("applation.message.name",null,locale);
	
	
    }
	
	
	
	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDTO> save(@RequestBody Book body) {
		
		return service.save(body);
	}

	@PutMapping(value = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDTO> update(@PathVariable("id") ID id, @RequestBody Book body) {
		return service.update(id, body);
	}

	@PatchMapping(value = "/{id}",consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDTO> patch(@PathVariable("id") Long id,  @RequestBody Book body) {
		return service.patch(id, body);
	}

	@DeleteMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseDTO> delete(@PathVariable("id") Long id) {
		
		return service.delete(id);
	}

	@GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<List<Book>> findAll(@PathVariable("id") ID id) {
		return service.findAll();
	}
	
	@GetMapping(value = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<Book> find(@PathVariable("id") Long id) {
		return service.find(id);
	}

}
